package de.schegge.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

class TargetTempDirTest {
    @Test
    void test(@TargetTempDir("test") Path temp) throws IOException {
        Assertions.assertEquals(Path.of("target"), temp.getParent());
        Files.writeString(temp.resolve("test.txt"), "Dies ist ein test");
        try (Stream<Path> list = Files.list(temp)) {
            Assertions.assertTrue(list.anyMatch(f -> f.endsWith("test.txt")));
        }
    }
}
