package de.schegge.test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(RandomValueResolver.class)
public class RandomValueResolverTest {
	@Test
	void randomize(@Randomize Boolean object) {
		assertNotNull(object);
	}

	@Test
	void randomize(@Randomize Byte object, @Randomize(min = 1, max = 2) byte primitive) {
		assertNotNull(object);
		assertNotEquals(0, primitive);
	}

	@Test
	void randomize(@Randomize Short object, @Randomize(min = 1, max = 2) short primitive) {
		assertNotNull(object);
		assertNotEquals(0, primitive);
	}

	@Test
	void randomize(@Randomize Integer object, @Randomize(min = 1, max = 2) int primitive) {
		assertNotNull(object);
		assertNotEquals(0, primitive);
	}

	@Test
	void randomize(@Randomize Long object, @Randomize long primitive) {
		assertNotNull(object);
		assertNotEquals(0, primitive);
	}

	@Test
	void randomize(@Randomize Float object) {
		assertNotNull(object);
	}

	@Test
	void randomize(@Randomize Double object) {
		assertNotNull(object);
	}

	@Test
	void randomize(@Randomize UUID value) {
		assertNotNull(value);
	}

	@Test
	void randomizeEnum(@Randomize StandardOpenOption open, @Randomize StandardCopyOption copy) {
		assertNotNull(open);
		assertNotNull(copy);
	}

	@Test
	void randomizeEnum(@Randomize(values = { "READ", "WRITE" }) StandardOpenOption open) {
		assertTrue(EnumSet.of(StandardOpenOption.READ, StandardOpenOption.WRITE).contains(open));
	}
}
