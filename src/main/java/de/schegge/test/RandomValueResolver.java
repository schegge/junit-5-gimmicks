package de.schegge.test;

import java.io.Serial;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

public class RandomValueResolver implements ParameterResolver {
	private static final List<? extends Class<?>> SUPPORTED_TYPES = Arrays.asList(Byte.class, byte.class, Short.class,
			short.class, Integer.class, int.class, Long.class, long.class, Float.class, float.class, Double.class,
			double.class, Boolean.class, boolean.class, UUID.class);

	private static class RandomGenerator extends Random {
		@Serial
		private static final long serialVersionUID = 1L;

		public short nextShort() {
			return (short) next(16);
		}

		public byte nextByte() {
			return (byte) next(8);
		}
	}

	private final RandomGenerator random = new RandomGenerator();

	@Override
	public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
			throws ParameterResolutionException {
		Parameter parameter = parameterContext.getParameter();
		return parameter.getAnnotation(Randomize.class) != null && isSupported(parameter.getType());
	}

	private boolean isSupported(Class<?> type) {
		return SUPPORTED_TYPES.contains(type) || type.isEnum();
	}

	@Override
	public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
			throws ParameterResolutionException {
		Class<?> type = parameterContext.getParameter().getType();
		if (type == Boolean.class || type == boolean.class) {
			return random.nextBoolean();
		}
		if (type == UUID.class) {
			return UUID.randomUUID();
		}
		if (type == Long.class || type == long.class) {
			return random.nextLong();
		}
		if (type == Float.class || type == float.class) {
			return random.nextFloat();
		}
		if (type == Double.class || type == double.class) {
			return random.nextDouble();
		}
		Randomize annotation = parameterContext.getParameter().getAnnotation(Randomize.class);
		if (type.isEnum()) {
			Object[] enumConstants = type.getEnumConstants();
			String[] values = annotation.values();
			if (values.length > 0) {
				return getEnum(type, values);
			}
			return enumConstants[random.nextInt(enumConstants.length)];
		}
		int range = annotation.min() == annotation.max() ? -1 : annotation.max() - annotation.min();
		if (type == Byte.class || type == byte.class) {
			return range == -1 ? random.nextByte() : (byte) randomRange(range, annotation.min(), Byte.MAX_VALUE);
		}
		if (type == Short.class || type == short.class) {
			return range == -1 ? random.nextShort() : (short) randomRange(range, annotation.min(), Short.MAX_VALUE);
		}
		if (type == Integer.class || type == int.class) {
			return range == -1 ? random.nextInt() : randomRange(range, annotation.min(), Integer.MAX_VALUE);
		}
		return null;
	}

	private int randomRange(int range, int min, int max) {
		if (range + min > max) {
			throw new ParameterResolutionException("invalud range");
		}
		return min + random.nextInt(range);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Object getEnum(Class<?> type, String[] values) {
		return Enum.valueOf((Class<? extends Enum>) type, values[random.nextInt(values.length)]);
	}
}