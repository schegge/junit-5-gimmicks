package de.schegge.test;

import com.google.common.jimfs.Jimfs;
import org.junit.jupiter.api.extension.AnnotatedElementContext;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.io.TempDirFactory;

import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;

public class InMemoryTempDirFactory implements TempDirFactory {
    private final FileSystem fileSystem = Jimfs.newFileSystem(com.google.common.jimfs.Configuration.unix());

    @Override
    public Path createTempDirectory(AnnotatedElementContext elementContext, ExtensionContext extensionContext) throws Exception {
        return Files.createTempDirectory(fileSystem.getPath("/"), "junit");
    }
}