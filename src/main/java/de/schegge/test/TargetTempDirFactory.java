
package de.schegge.test;

import org.junit.jupiter.api.extension.AnnotatedElementContext;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.io.TempDirFactory;

import java.nio.file.Files;
import java.nio.file.Path;

class TargetTempDirFactory implements TempDirFactory {
    private static final Path TARGET = Path.of("target");
    @Override
    public Path createTempDirectory(AnnotatedElementContext elementContext, ExtensionContext extensionContext) throws Exception {
        Path target = elementContext.findAnnotation(TargetTempDir.class)
                .map(TargetTempDir::value).map(Path::of)
                .filter(p -> !p.isAbsolute()).map(TARGET::resolve).orElse(TARGET);
        return Files.createDirectories(target);
    }
}